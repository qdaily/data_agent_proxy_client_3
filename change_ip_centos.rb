require 'net/http'
require 'socket'
require 'json'

HOST = 'test.dataagent.somecoding.com'
PORT = '80'
VERSION = '1.0.0'

def run
  process_count = `ps aux|grep change_ip|grep ruby | grep bash| egrep -v 'grep' |wc -l`
  return if process_count.to_i > 1

  20.times do |i|
    adsl_connect unless `route -n`.include?("ppp0")
    return unless `route -n`.include?("ppp0")
    if account_id = check_status
      adsl_connect
      update_ip(account_id)
    end
    sleep 5
  end
end

def adsl_connect
  result = false
  begin
    Timeout.timeout(30, Errno::ETIMEDOUT) do
      `pppoe-stop`
      sleep 1
      #开启vps拨号
      `pppoe-start`
      20.times do |i|
        if `route -n`.include?("ppp0")
          result = true
          break
        end
        sleep 1
      end
      raise "vps拨号 start failed" unless result
    end
  rescue => e
    puts "启动vps拨号失败：#{e}".red
    result = false
  end
  `pppoe-stop` if result == false
  return result
end

def check_status
  begin
    res = Net::HTTP.start(HOST, PORT) {|http| http.get("/api/vps_accounts/show_by_proxy_client_title?proxy_client_title=#{Socket.gethostname}&proxy_client_version=#{VERSION}")}
    data = JSON.parse res.body
    return false unless data["account"]["proxy_client_status"] == 'needupdate'
    account_id = data["account"]["account_id"]
  rescue => e
    i ||= 0
    i += 1
    retry if i < 3
    adsl_connect
    sleep 5
    false
  end
end

def update_ip(account_id)
  begin
    res = Net::HTTP.start(HOST, PORT) {|http| http.post("/api/vps_accounts/update_url", "account_id=#{account_id}")}
  rescue => e
    i ||= 0
    i += 1
    retry if i < 3
    false
  end
end

run
