require 'net/http'
require 'socket'
require 'json'

HOST = 'test.dataagent.somecoding.com'
PORT = '80'
VERSION = '1.0.0'

def run
	while true
		break if Time.now.sec > 50
		adsl_connect unless TCPSocket.gethostbyname(Socket.gethostname)[4]
		break unless TCPSocket.gethostbyname(Socket.gethostname)[4]
		if account_id = check_status
			adsl_connect
			update_ip(account_id)
		end
		sleep 5
	end
end

def adsl_connect
	begin
		Timeout.timeout(20, Errno::ETIMEDOUT) do 
			`C:/data_agent_proxy_client_3/ADSL.bat`
		end
	rescue => error
		i ||= 0
		i += 1
		retry if i < 3
	end
end

def check_status
	begin
		res = Net::HTTP.start(HOST, PORT) {|http| http.get("/api/vps_accounts/show_by_proxy_client_title?proxy_client_title=#{Socket.gethostname}&proxy_client_version=#{VERSION}")}
		data = JSON.parse res.body
		return false unless data["account"]["proxy_client_status"] == 'needupdate'
		account_id = data["account"]["account_id"]
	rescue => e
		i ||= 0
		i += 1
		retry if i < 3
		adsl_connect
		sleep 5
		false
	end
end

def update_ip(account_id)
	begin
		res = Net::HTTP.start(HOST, PORT) {|http| http.post("/api/vps_accounts/update_url", "url=#{TCPSocket.gethostbyname(Socket.gethostname)[4]}&account_id=#{account_id}")}
	rescue => e
		i ||= 0
		i += 1
		retry if i < 3
		false
	end
end

run
