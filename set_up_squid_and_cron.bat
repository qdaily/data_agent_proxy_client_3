@echo off
echo 开始初始化启动squid
cd c:\squid\sbin
squid -i
squid -z
net start squid
echo squid启动完成
echo 配置定时任务
for /f "tokens=1* delims==" %%i in (c:\data_agent_proxy_client_3\config.txt) do if "%%i"=="user" set user=%%j
for /f "tokens=1* delims==" %%i in (c:\data_agent_proxy_client_3\config.txt) do if "%%i"=="password" set password=%%j
schtasks /create /sc minute /mo 1 /RU %user% /RP %password% /tn "change_ip" /tr "c:\data_agent_proxy_client_3\change_ip.rb"
schtasks /create /sc hourly /mo 6 /RU %user% /RP %password% /tn "restart_pc" /tr "c:\data_agent_proxy_client_3\restart.rb"
echo 配置定时任务完成
echo.
