# README #

Data_agent IP 客户端，用于搭建了代理的VPS端拨号上网变换IP，同时将新的IP更新至总控端，便于总控端分配资源。

### How to set up? ###

* 安装rubyinstaller-1.9.3-p551.exe
* 解压squid包到C盘根目录
* clone本项目至C盘根目录，并进入项目目录
* 在config.txt中填入必要信息
* 确保change_ip.rb中HOST正确
* 双击执行change_host_name.bat文件，将从config中读取计算机名并修改。（过十几秒会重启）。
* 重启后再次进入醒目目录，双击set_up_squid_and_cron.bat，启动squid并配置定时任务。
